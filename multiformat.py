# should install 
# pip install datefinder
import datefinder
import functions as function
import const as co
import re 


def month_processor(month):

    # print("heloo month processor")
    # print(month)
    data = co.dec_month[str(month)]
    return data


# ------------------------------------------------------------------
# ------------------------------------------------------------------


def date_processor(text):
    # print("heloo date processor")
    try:
        # print("from the try")
        match = datefinder.find_dates(text,source=1)
        # print("heloo after match")
        for item in match:
            # item:(datetime.datetime(2005, 5, 5, 0, 0), '5/5/2005')
            # print(item)
            # item[1]: 5/5/2005
            # print(item[1])
            #item[0]  2005-05-05 00:00:00
            # print(item[0])
            replacement = ""

            if item[1].__len__() > 4:
                replacement += function.number_to_word(str(item[0].day))
                # print(item[0].month)
                replacement += month_processor(str(item[0].month))
                # print(replacement)
                replacement += " "
            replacement += function.number_to_word(str(item[0].year))
            text = text.replace(item[1], replacement)
            
            
    except:
        pass

    return text




# ------------------------------------------------------------------
# ------------------------------------------------------------------




# text="hollo in 5/05/2005"
# ans=date_processor(text)
# print(ans)    




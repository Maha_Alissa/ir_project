import first as q1
import limtizer as q2
import multiformat as q3
import Abbreviations as q5
def text_processor(text):
    text  = q3.date_processor(text)
    text =  q1.remove_punctuation(text)
    text =  q5.reblaceAbbreviations(text)
    text =  q1.convert_signs_to_words(text)
    text =  q1.convert_number_to_word(text)
    text =  q1.split_dash_expression(text)
    mylist = q1.tokenize_text(text)
    mylist = q2.lemmatize_verb(mylist)
    mylist = q1.to_lower(mylist)
    mylist = q1.remove_stopwords(mylist)
    mylist = q1.stem_words(mylist)

    # print(mylist)
    return mylist
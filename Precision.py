import Myfunctions as myFu
import Index as q6
import textProcessor as pro
import matching as q7
import pickle

# noti with cacm   Rq=RqDic[i] without str
 
#  For a query 𝑞 with known set of relevant documents ℛ�
#  Run the system and let 𝐴𝑞 be the response of the system
#  Might be the top 10 answers
# Aq is the System respons
# Rq is the correct answer
# Aq^Rq/Aq

#5  6  7 8 14 22
#  in this  file we calculate the Precision for all the querys in the two dataSet
#  creat dictionary for store query id and the precision 
#  read the store dictionary for query
numQueryCISI=112
numQueryCACM=64
CISI='CISI'
CACM='CACM'


# Aq^Rq/Aq
# 𝑃 (𝑟𝑒𝑙𝑒𝑣𝑎𝑛𝑡/𝑟𝑒𝑡𝑟𝑖𝑒𝑣𝑒𝑑)
precisionDic={}
def intersection(lst1, lst2):
    lst3 = [value for value in lst1 if value in lst2]
    return lst3

def calcPrecision(numOfQuery,dataSetName):
    myDecQuery={}
    RqDic={}
    myindex = q6.read_index(dataSetName)
    myDecQuery=myFu.readQueryDec(dataSetName)
    RqDic=myFu.readRelDec(dataSetName)
    print(RqDic)
    for i in range(1,numOfQuery+1):
     print(i)
     Aq=[]
     Rq=[]
     if str(i) in myDecQuery:
        print("in the if")
        query=myDecQuery[str(i)]
        listquery = pro.text_processor(query)  
        Aq = q7.match(listquery, myindex,dataSetName) 
        print(Aq)
        # if str(i) in RqDic.keys():
        #     print("in the if 2222")
        
        try:
                Rq=RqDic[i]
                print(Rq)
                print(Rq.__len__())
                # print("///////////////////////////////////////////////////////")
                # print(Aq)
                intersectionList=intersection(Aq,Rq)
                precision=intersectionList.__len__()/Aq.__len__()
                print("++++++++++++++++precision++++++++++++++++++")
                print(precision)
                precisionDic[str(i)]=precision
        except:
            pass        

    with open("Evaluation/Precision/"+str(dataSetName)+".pickle", "wb") as outp:
       pickle.dump(precisionDic,outp,pickle.HIGHEST_PROTOCOL)    
        



def calcPrecision10(numOfQuery,dataSetName):
    myDecQuery={}
    RqDic={}
    myindex = q6.read_index(dataSetName)
    myDecQuery=myFu.readQueryDec(dataSetName)
    RqDic=myFu.readRelDec(dataSetName)
    for i in range(1,numOfQuery+1):
     print(i)
     Aq=[]
     Rq=[]
     if str(i) in myDecQuery:
        print("in the if")
        query=myDecQuery[str(i)]
        listquery = pro.text_processor(query)  
        Aq = q7.match(listquery, myindex,dataSetName) 
        Aq10=Aq[:10]
        # if int(i) in RqDic:
        try:
            Rq=RqDic[i]
            print(Rq.__len__())
            intersectionList=intersection(Aq10,Rq)
            precision=intersectionList.__len__()/Aq10.__len__()
            print("++++++++++++++++precision10++++++++++++++++++")
            print(precision)
            precisionDic[str(i)]=precision
        except:
            pass     

    with open("Evaluation/Precision10/"+str(dataSetName)+".pickle", "wb") as outp:
       pickle.dump(precisionDic,outp,pickle.HIGHEST_PROTOCOL)
# one run
# ----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
calcPrecision(numQueryCISI,CISI)
calcPrecision(numQueryCACM,CACM)
calcPrecision10(numQueryCISI,CISI)
calcPrecision10(numQueryCACM,CACM)




        





        



        


    



    


    



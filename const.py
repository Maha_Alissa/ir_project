dec_month={
    "1": " january ",
    "2": " february ",
    "3": " march ",
    "4": " april ",
    "5": " may ",
    "6": " june ",
    "7": " july ",
    "8": " august ",
    "9": " september ",
    "10": " october ",
    "11": " november ",
    "12": " december "
}
# print(dec_month["5"])
dec_abbreviated={
    "ex":"example",
    "eg":"example",
    "feb":"february",
    "dec": "december",
    "su":"sunday",
    "mo":"monday",
    "USA":"The United States of America",
    "2day":"today",
    "cu":"see you",
    "Amt" : "amount",
    "EOD":"End Of Day",
    "ITS"  :  "Intelligent Tutoring System",
    "IVRAI"	:"Interactive Voice Response Artificial Intelligence",
    "IVRAI"	:"Immersive Virtual Reality Artificial Intelligence",
    "IWAI":	"International Workshop on Artificial Intelligence",
    "JITS":	"Java Intelligent Tutoring System",
    "LAIP"	:"Linking Artificial Intelligence Principles",
    "LASP"	:"Layered Shortest Path Routing computer algorithm",
    "LLML"	:"Life Long Machine Learning",
    "LML"	:"Link Machine Learning",
    "LNCHR":	"Launcher",
    "LSML":	"Large Scale Machine Learning",
    "LTTT":"	Long-Term Turing Test (Artificial Intelligence)",
    "MARI"	:"Microsoft Africa Research Center",
    "MDAI"	:"Modeling Decisions for Artificial Intelligence",
    "MDAI"	:"Modelling Decisions for Artificial Intelligence",
    'MFAI'	:"Mathematical Foundations of Artificial Intelligence",
    'MIQ'	:"Machine Intelligence Quotient",
    'MLADS':	"Machine Learning AI Data Science",
    'MLAO':"	Machine Learning Assisted Optimization",
    'MLBB'	:"Machine Learning in Biomedicine and Bioinformatics",
    'MLI'	:"Machine Learning Interpretability",
    'MLIF':	"Machine Learning Is Fun",
    'MLIP':	"Machine Learning and Image Processing",
    'MLLS':	"Machine Learning in Life Science",
    "041"	:"It's an Isaac thing, Mr ndEti",
    "0DLRA"	:"Zero Data Loss Recovery Appliance"   ,
    "0DOF":	"Zero Degree of Freedom" ,  
    "0WS"	:"Zero Wait State"    ,
    "100E2R":	"One Hundred Percent Easy to Read"  ,
    "10GE":	"Ten Gigabit Ethernet"  ,
    "10GEA"	:"Ten Gigabit Ethernet Alliance"  ,
    "1A4":	"420 in Hexidecimal"   ,
    "1CA"	:"One Click Application"   ,
    "1CB"	:"One Click Backup"   ,
    "1CU":	"One Click Unsubscribe"   ,
    "1DOF":	"Single Degree of Freedom"   ,
    "1GE":	"One Port Gigabit Ethernet"   ,
    "1GIP":	"First Generation Internet Protocol"   ,
    "1GL"	:"First General Language"  , 
    "1GS":"	First Generation Security"   ,
    "1HW":	"One Hacker Way "  ,
    "1ISC":	"One Instruction Set Computer"   ,
    "1MW":"One Microsoft Way"   ,
    "1NF":"First Normal Form"   ,
    "1RM":"One Repetition Maximum"   ,
    "1RU":"Single Rack Unit"   ,
    "1SR" : "One Shot Release"   ,
    "1TP": "One Time Password"   ,
    "1V1": "One Versus One "    ,
    "AR":"Argentina"	,
    "HN":"Honduras",
	"PE":"Peru" ,
    "AU":"Australia" ,
    "HK":"Hong Kong" ,
    "PH":"Philippines" ,
    "AT":"Austria" ,
	"HU":"Hungary" ,
    "PL":"Poland" ,
    "BS":"Bahamas" ,
	"IS":"Iceland" ,
    "PT":"Portugal" ,
    "BH":"Bahrain" ,
	"IN":"India" 
}
# MRRCISI
# 76
# 53.29087374785909
# MRR
# 0.7011957072086722
# ------------------------
# 52
# 41.31158010754935
# Mrr
# 0.7944534636067182



MAP_MRR={
"CISIMAP":0.0308010281887786,
"CACMMAP":0.00982996833332746,
"MRRCISI":0.7011957072086722,
"MRRCACM":0.7944534636067182
}
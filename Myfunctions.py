import pickle

def readRelDec(corbasName)   :    
   with open("Query/Rel/"+str(corbasName)+"Rel.pickle", "rb") as inpRel:
     rel=pickle.load(inpRel)
   return rel
# ---------------------------------------------------------------------------------
# test
# ---------------------------------------------------------------------------------
# myDec={}
# myDec=readRelDec(corbasName)
# print(myDec['1'])
# list=myDec['1']
# print(list.__len__())

# ----------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------
def readQueryDec(corbasName)   :    
   with open("Query/QueryDic/"+str(corbasName)+"Query.pickle", "rb") as inpQuery:
     rel=pickle.load(inpQuery)
   return rel
    
# ---------------------------------------------------------------------------------
# one run
# ---------------------------------------------------------------------------------

# readQueryDec('CISI')
# readQueryDec('CACM')


# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------
def readPrecision(corbasName):
   with open("Evaluation/Precision/"+str(corbasName)+".pickle", "rb") as pres:
     pre= pickle.load(pres)
   return pre 


# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------
def readPrecision10(corbasName):
   with open("Evaluation/Precision10/"+str(corbasName)+".pickle", "rb") as pres10:
     pre= pickle.load(pres10)
   return pre      

# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------
def readrecall(corbasName):
   with open("Evaluation/Recall/"+str(corbasName)+".pickle", "rb") as pres10:
     pre= pickle.load(pres10)
   return pre   
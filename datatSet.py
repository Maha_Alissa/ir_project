# import numpy as np # linear algebra
# import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
# import os
#C:\Users\Maha\Desktop\IRProject\dataSet\
with open('dataSet/cacm.all') as f:
    lines = ""
    for l in f.readlines():
        lines += "\n" + l.strip() if l.startswith(".") else " " + l.strip()
    lines = lines.lstrip("\n").split("\n")
    
# print n lines
n = 5
for l in lines[:n]:
    print(l)
print('/////////////////////////////////////////')
doc_set = {}
doc_id = ""
doc_auther=""
doc_text = ""
for l in lines:
    if l.startswith(".I"):
        doc_id = l.split(" ")[1].strip()
    elif l.startswith(".X"):
        doc_set[doc_id] = doc_text.lstrip(" ")

        with open("dataSet/CACMDataSet/"+doc_id+".txt", 'w') as f:
          f.write(doc_set[doc_id])
        doc_id = ""
        doc_text = ""    

    else:
        doc_text += l.strip()[3:] + " " # The first 3 characters of a line can be ignored.

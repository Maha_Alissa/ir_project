import Myfunctions as myFu
import Index as q6
import textProcessor as pro
import matching as q7
import pickle


#  recall=Aq^Rq/Rq
# 𝑃 (𝑟𝑒𝑡𝑟𝑖𝑒𝑣𝑒𝑑/𝑟𝑒𝑙𝑒𝑣𝑎𝑛t)
numQueryCISI=112
numQueryCACM=64
CISI='CISI'
CACM='CACM'
recallDic={}
def intersection(lst1, lst2):
    lst3 = [value for value in lst1 if value in lst2]
    return lst3

def calcRecall(numOfQuery,dataSetName):
    myDecQuery={}
    RqDic={}
    myindex = q6.read_index(dataSetName)
    myDecQuery=myFu.readQueryDec(dataSetName)
    RqDic=myFu.readRelDec(dataSetName)
  
    for i in range(1,numOfQuery+1):
     print(i)
     Aq=[]
     Rq=[]
     if str(i) in myDecQuery:
        print("in the if")
        query=myDecQuery[str(i)]
        listquery = pro.text_processor(query)  
        Aq = q7.match(listquery, myindex,dataSetName) 
        if str(i) in RqDic:
            Rq=RqDic[str(i)]
            print(Rq.__len__())
            # print("///////////////////////////////////////////////////////")
            # print(Aq)
            intersectionList=intersection(Aq,Rq)
            recall=intersectionList.__len__()/Rq.__len__()
            print("++++++++++++++++recall++++++++++++++++++")
            print(recall)
            recallDic[str(i)]=recall

    with open("Evaluation/Recall/"+str(dataSetName)+".pickle", "wb") as outp:
       pickle.dump(recallDic,outp,pickle.HIGHEST_PROTOCOL)  


# --------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------

def calcRecallCACM(numOfQuery,dataSetName):
    myDecQuery={}
    RqDic={}
    myindex = q6.read_index(dataSetName)
    myDecQuery=myFu.readQueryDec(dataSetName)
    RqDic=myFu.readRelDec(dataSetName)
  
    for i in range(1,numOfQuery+1):
     print(i)
     Aq=[]
     Rq=[]
     if str(i) in myDecQuery:
        print("in the if")
        query=myDecQuery[str(i)]
        listquery = pro.text_processor(query)  
        Aq = q7.match(listquery, myindex,dataSetName) 
        try:
            Rq=RqDic[i]
            print(Rq.__len__())
            # print("///////////////////////////////////////////////////////")
            # print(Aq)
            intersectionList=intersection(Aq,Rq)
            recall=intersectionList.__len__()/Rq.__len__()
            print("++++++++++++++++recall++++++++++++++++++")
            print(recall)
            recallDic[str(i)]=recall
        except:
            pass     

    with open("Evaluation/Recall/"+str(dataSetName)+".pickle", "wb") as outp:
       pickle.dump(recallDic,outp,pickle.HIGHEST_PROTOCOL)  







#    one run 
# --------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------
# calcRecall(numQueryCISI,CISI)
calcRecallCACM(numQueryCACM,CACM)          
        
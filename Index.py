import pickle
import math
import dill

# كل سطر بيحوي على التيرم و تكراراها و مؤشرات على كل دوكيومنت بيحوي هذه الكلمة  
class row:
    term = ""
    freq = 1
    tf = 0
    idf = 0
    tf_idf = 0
    pointers = []
#------------------------------------------------
#------------------------------------------------
#بالتابع هاد بعبي الأسطر بالتيرم ومعلوماتها
    def __init__(self,word="",freq=1,tf=0):
        self.term = word
        self.freq = freq
        self.tf = tf
        self.idf = 0
        self.tf_idf = 0
        self.pointers = []
    
#------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------
class bag:
    docNum = 0
    rows = []
#------------------------------------------------
#------------------------------------------------
    def __init__(self,docNum,lista):
        self.docNum = docNum
        self.rows = []

        terms = []

        for item in lista:
            if terms.__contains__(item) == 1:
                self.increase_row_freq(item)

            else:
                terms.append(item)
                terms.sort()
                order = terms.index(item)
                self.rows.insert(order,row(item))

        self.calc_tf()
#------------------------------------------------
#------------------------------------------------
    def find_row(self,word):
        for item in self.rows:
            if item.term == word:
                return item
        return row

#------------------------------------------------
#------------------------------------------------
    def increase_row_freq(self,word):
        row = self.find_row(word)
        row.freq += 1

#------------------------------------------------
#------------------------------------------------
    def calc_tf(self):
        for row in self.rows:
            row.tf = row.freq / self.rows.__len__()

#------------------------------------------------
#------------------------------------------------
    def calc_tf_idf(self,idfDict):
        for row in self.rows:
            print("For BOW: "+str(self.docNum)+", Calculating TF-IDF: "+str(row.term))
            try:
                row.tf_idf = row.tf * idfDict[row.term]
            except:
                row.tf_idf = 0

#------------------------------------------------

    def write_bag_file(self,corbasName):
        # with open("utilities/bags/CACMpickle/"+str(self.docNum)+".pickle", "wb") as outp:
        with open("corbas/"+str(corbasName)+"/bags/pickle/"+str(self.docNum)+".pickle", "wb") as outp:
         pickle.dump(self,outp,pickle.HIGHEST_PROTOCOL)
        # file = open("utilities/bags/CACMtxt/"+str(self.docNum)+".txt", "w")
        file = open("corbas/"+str(corbasName)+"/bags/txt/"+str(self.docNum)+".txt", "w")
        for row in self.rows:
            file.writelines(row.term+" | "+str(row.freq)+" | "+str(row.tf)+" | "+str(row.tf_idf)+"\n")
        file.close()

#------------------------------------------------

    def write_bag_file(self):
        dill.dump(self, file = open("utilities/bags/walaapickle/"+str(self.docNum)+".pickle", "wb"))
        file = open("utilities/bags/walaatxt/"+str(self.docNum)+".txt", "w")
        for row in self.rows:
            file.writelines(row.term+" | "+str(row.freq)+" | "+str(row.tf)+" | "+str(row.tf_idf)+"\n")
        file.close()

#-------------------------------------------------------


    def write_cbag_file(self):
        pickle.dump(self, file = open("utilities/bags/clusterpickle/"+str(self.docNum)+".pickle", "wb"))
        file = open("utilities/bags/clustertxt/"+str(self.docNum)+".txt", "w")
        for row in self.rows:
            file.writelines(row.term+" | "+str(row.freq)+" | "+str(row.tf)+" | "+str(row.tf_idf)+"\n")
        file.close()
        #---------------------------------------------#

    def write_clusterbag_file(self):
        with open("utilities/bags/clusterpickle/"+str(self.docNum)+".pickle", "wb") as outp:
        # with open("utilities/bags/pickle/"+str(self.docNum)+".pickle", "wb") as outp:
         pickle.dump(self,outp,pickle.HIGHEST_PROTOCOL)
        file = open("utilities/bags/clustertxt/"+str(self.docNum)+".txt", "w")
        # file = open("utilities/bags/txt/"+str(self.docNum)+".txt", "w")
        for row in self.rows:
            file.writelines(row.term+" | "+str(row.freq)+" | "+str(row.tf)+" | "+str(row.tf_idf)+"\n")
        file.close()
#-----------------------------------------------------------------#



    def print_bag(self):
        print("Doc | Word  | Frequency | TF | TF-IDF")
        for row in self.rows:
            print(row.term+" | "+str(row.freq)+" | "+str(row.tf)+" | "+str(row.tf_idf))

####################################################################################################
class index:
    
    rows = []
    idfDict = dict.fromkeys([''],0)

#-----------------------------------------------
# ----------------------------------------------

    def __init__(self,bagsList):
        self.rows = []
        self.idfDict = dict.fromkeys([''],0)
        words = []
        i = 1
        for bag in bagsList:
            print('Merging: '+str(i))
            i += 1
            for innerRow in bag.rows:
                if words.__contains__(innerRow.term):
                    self.edit_row(innerRow.term, innerRow.freq, bag.docNum)
                else:
                    words.append(innerRow.term)
                    words.sort()
                    order = words.index(innerRow.term)
                    newRow = row(innerRow.term,innerRow.freq)
                    newRow.pointers.append(bag.docNum)
                    self.rows.insert(order,newRow)
        self.calc_idf(words,bagsList.__len__())
#------------------------------------------------
    def find_row(self,word):
        for item in self.rows:
            if item.term == word:
                return item
        return row

#------------------------------------------------
    def edit_row(self,word,freq,docNum):
        row = self.find_row(word)
        row.freq += freq
        if row.pointers.__contains__(docNum) == 0:
            row.pointers.append(docNum)
#------------------------------------------------
# calculate idf
#------------------------------------------------
    def calc_idf(self,wordsList,corbasCount):
        self.idfDict=dict.fromkeys(wordsList,0)
        for row in self.rows:
            print("Calculating IDF: "+row.term)
            # Rowdf= float(row.freq)/row.pointers.__len__()
            row.idf = math.log(corbasCount/row.pointers.__len__())
            self.idfDict[row.term] = row.idf
#------------------------------------------------
#------------------------------------------------
    def print_index(self):
        print("Word | Frequency | IDF | Pointers")
        for row in self.rows:
            print(row.term+" | "+str(row.freq)+" | "+str(row.idf)+" | "+str(row.pointers))  

#------------------------------------------------
    def write_index_file(self,corbasName):
        # with open("utilities/indexCACM.pickle", "wb") as outp:
        with open("corbas/"+str(corbasName)+"/index.pickle", "wb") as outp:
         pickle.dump(self,outp,pickle.HIGHEST_PROTOCOL)
        # dill.dump(self, file = open("utilities/index.pickle", "wb"))
        file = open("corbas/"+str(corbasName)+"/index.txt", "w")
        # file = open("utilities/indexCACM.txt", "w")
        for row in self.rows:
            file.writelines(row.term+" | "+str(row.freq)+" | "+str(row.idf)+" | "+str(row.pointers)+"\n")
        file.close()

#-----------------------------------------------------------
    def write_index_file(self):
        dill.dump(self, file = open("utilities/indexWALAA.pickle", "wb"))
        file = open("utilities/indexWALAA.txt", "w")
        for row in self.rows:
            file.writelines(row.term+" | "+str(row.freq)+" | "+str(row.idf)+" | "+str(row.pointers)+"\n")
        file.close()

#-----------------------------------------------------------
    def write_clustindex_file(self):
        pickle.dump(self, file = open("utilities/clustindex.pickle", "wb"))
        file = open("utilities/clustindex.txt", "w")
        for row in self.rows:
            file.writelines(row.term+" | "+str(row.freq)+" | "+str(row.idf)+" | "+str(row.pointers)+"\n")
        file.close()   

#--------------------------------------------------------
def read_wbag(docNum):
    return dill.load(open("utilities/bags/walaapickle/"+str(docNum)+".pickle", "rb"))
#--------------------------------------------------

def read_bag(docNum,corbasName):
 with open("corbas/"+str(corbasName)+"/bags/pickle/"+str(docNum)+".pickle", "rb") as inp:
#  with open("utilities/bags/CACMpickle/"+str(docNum)+".pickle", "rb") as inp:
     mybag=pickle.load(inp)
 return mybag
#--------------------------------------------
def read_index(corbasName):
    print('i am  read the index')
    
    return pickle.load(open("corbas/"+str(corbasName)+"/index.pickle", "rb"))
#--------------------------------------------------
def read_clustbag(docNum):
    return pickle.load(open("utilities/bags/clusterpickle/"+str(docNum)+".pickle", "rb"))

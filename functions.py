# should install inflect
# pip install inflect
# from operator import getitem
import inflect

def number_to_word(text):
    # print("hi from n to w")
    inflector = inflect.engine()
    temp_str = text.split()
    new_string = []
    for word in temp_str:
        # print(word)
        # campare a char
        if word.__getitem__(0).isdigit():
            word = word.replace(",", "")
            if word.isdigit():
                temp = inflector.number_to_words(word)
                new_string.append(temp)
        else:
            new_string.append(word)
  
    temp_str = ' '.join(new_string)
    return temp_str
# ------------------------------------------------------------------
# test
# ------------------------------------------------------------------

# str=5
# sss=number_to_word(str)
# print(sss)
# ------------------------------------------------------------------
# ------------------------------------------------------------------

def read_file(filePath):
    file = open(filePath, "r")
    content = file.read()
    file.close()
    return content
# ------------------------------------------------------------------
# ------------------------------------------------------------------    

def to_lower(lista):
    outputLista = []
    for word in lista:
        outputLista.append(word.lower())
  
    return outputLista


# ------------------------------------------------------------------
# ------------------------------------------------------------------











# ------------------------------------------------------------------
# ------------------------------------------------------------------

# inflect.py - Correctly generate plurals, singular nouns, ordinals, indefinite articles; convert numbers to words.
# plural plural_noun plural_verb plural_adj singular_noun no num
# compare compare_nouns compare_nouns compare_adjs
# a an
# present_participle
# ordinal number_to_words
# join
# inflect classical gender
# defnoun defverb defadj defa defa


# ------------------------------------------------------------------
# ------------------------------------------------------------------

# split:
# txt = "i'm a student"
# x = txt.split()
# print(x)
# output:["i'm", 'a', 'student']

# ------------------------------------------------------------------
# ------------------------------------------------------------------

# function getitem

# ------------------------------------------------------------------
# ------------------------------------------------------------------


# a = ["apple", "banana", "cherry"]
# b = ["Ford", "BMW", "Volvo"]
# a.append(b)
# print(a)
# output:['apple', 'banana', 'cherry', ["Ford", "BMW", "Volvo"]]

# ------------------------------------------------------------------
# ------------------------------------------------------------------
# myTuple = ("John", "Peter", "Vicky")
# x = "#".join(myTuple)
# print(x)

# output:John#Peter#Vicky



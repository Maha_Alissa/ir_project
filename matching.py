import math
import pickle
import Index as q6
import matchingMethod as q7v1
####################################################################################################
def creat_vectors(query,matchingFiles,index,corbasName):
    queryBag = q6.bag(0,query)
    queryBag.calc_tf_idf(index.idfDict)
    queryVector = []

    for index_row in index.rows:
        row = queryBag.find_row(index_row.term)
        queryVector.append(row.tf_idf)
    
    bagsVectors = []

    for fileNum in matchingFiles:
        vector = read_vector(fileNum,corbasName)
        bagsVectors.append(vector)
    # print(bagsVectors.__len__())
    # bagsVectors.sort(reverse=True)
    # myvector=bagsVectors[0:200]
    # print(myvector.__len__())
    return queryVector,bagsVectors
#----------------------------------------------------------------------------------------------- 

def creat_vectors1(query,matchingFiles,index):
    queryBag = q6.bag(0,query)
    queryBag.calc_tf_idf(index.idfDict)
    queryVector = []

    for index_row in index.rows:
        row = queryBag.find_row(index_row.term)
        queryVector.append(row.tf_idf)
    
    bagsVectors = []

    for fileNum in matchingFiles:
        vector = read_vector1(fileNum)
        bagsVectors.append(vector)
    print("mahooooooooooooooooosh the bag vector len")  
    print(bagsVectors.__len__())
    # bagsVectors.sort(reverse=True)
    # myvector=bagsVectors[0:200]
    # print(myvector.__len__())
    return queryVector,bagsVectors


# Similarity = (A.B) / (||A||.||B||)

# Cosine Similarity (d1, d2) = Dot product(d1, d2) / ||d1|| * ||d2||

# Dot product (d1,d2) = d1[0] d2[0] + d1[1] d2[1] … d1[n] * d2[n]

# ||d1|| = square root(d1[0]2 + d1[1]2 + ... + d1[n]2)//the norm ∥x∥=x21+x22+⋯+x2n√.

# ||d2|| = square root(d2[0]2 + d2[1]2 + ... + d2[n]2)
#------------------------------------------------------------------------------------------------
def cosineSimilarity(queryVector,bagsVectors):
    cosDict=dict.fromkeys('',0)
    result = []
    normQuery = 0
    for i in range(0,queryVector.__len__()):
        normQuery += queryVector.__getitem__(i) * queryVector.__getitem__(i)

    for j in range(0,bagsVectors.__len__()):
        Dotproduct = 0
        dotNorm = 0
        normDocument = 0
        for i in range(0,queryVector.__len__()):
            Dotproduct += bagsVectors.__getitem__(j).myvector.__getitem__(i) * queryVector.__getitem__(i)
            normDocument += bagsVectors.__getitem__(j).myvector.__getitem__(i) * bagsVectors.__getitem__(j).myvector.__getitem__(i)
        dotNorm = math.sqrt(normDocument * normQuery)
        similarity = 0
        if dotNorm != 0:
            similarity = Dotproduct / dotNorm
        cosDict.update({bagsVectors.__getitem__(j).docNum:similarity})
# to sort by value// reverse true to sorted descending
    cosDict = dict(sorted(cosDict.items(), key=lambda item: item[1], reverse = True))
    # print(cosDict)

    # result = list(cosDict.keys())[:10] 
    result = list(cosDict.keys())

    return result

#--------------------------------------------------------------------------------
# -------------------------------------------------------------------------------



def find_matching_corpus(query,index):
      matchingFiles = []
    
      for row in index.rows:
            # print(row.term)
            if query.__contains__(row.term) == 1:
                for fileNum in row.pointers:
                    if matchingFiles.__contains__(fileNum) == 0:
                        matchingFiles.append(fileNum)

      return    matchingFiles
    #   -----------------------------------------------------

       
# -----------------------------------------------------------------------------  
def match(query,index,corbasName):
   
    matchingFiles = find_matching_corpus(query,index)
    queryVector,bagsVectors = creat_vectors(query,matchingFiles,index,corbasName)
    # cosineSimilarity(queryVector,bagsVectors)
    return cosineSimilarity(queryVector,bagsVectors)

# -----------------------------------------------------------------------------

def match1(query,index):
    matchingFiles = find_matching_corpus(query,index)
    queryVector,bagsVectors = creat_vectors1(query,matchingFiles,index)
    # cosineSimilarity(queryVector,bagsVectors)
    return cosineSimilarity(queryVector,bagsVectors)
# -----------------------------------------------------------------------------
# class Vector
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------


class vector():
    docNum = -1
    myvector = -1
    corbasName=""

#------------------------------------------------
    def __init__(self,docNum,index,corbasName):
        self.docNum = docNum
        self.myvector = self.calc_vector(index,corbasName)

#------------------------------------------------
    def calc_vector(self,index,corbasName):
        print("Vectoring: "+str(self.docNum))
        bag = q6.read_bag(self.docNum,corbasName)
        vector = []
        for index_row in index.rows:
            row = bag.find_row(index_row.term)
            vector.append(row.tf_idf)
        return vector
    #---------------------------------------------------------------- 
    # def getTfIdf(self,)

# ----------------------------------------------------------
    def write_vector_file(self,corbasName):
      with   open("corbas/"+str(corbasName)+"/vectors/"+str(self.docNum)+".pickle", "wb") as file:
        pickle.dump(self, file ,pickle.HIGHEST_PROTOCOL)

#---------------------------------------------------------
    def write_clustvector_file(self):
      with   open("utilities/vectorcluster/"+str(self.docNum)+".pickle", "wb") as file:
    #   with   open("utilities/vectors/"+str(self.docNum)+".pickle", "wb") as file:
        pickle.dump(self, file ,pickle.HIGHEST_PROTOCOL)



####################################################################################################
def read_vector(docNum,corbasName):
 
    return pickle.load(open("corbas/"+str(corbasName)+"/vectors/"+str(docNum)+".pickle", "rb"))
#--------------------------------------------------------

def read_vector1(docNum):
    return pickle.load(open("utilities/vectorcluster/"+str(docNum)+".pickle", "rb"))
    # return pickle.load(open("utilities/vectors/"+str(docNum)+".pickle", "rb"))
       

class vector1():
    docNum = -1
    myvector = -1

#------------------------------------------------#
    def __init__(self,docNum,index):
        self.docNum = docNum
        self.myvector = self.calc_vector(index)

#------------------------------------------------#
    def calc_vector(self,index):
        print("Vectoring: "+str(self.docNum))
        bag = q6.read_clustbag(self.docNum)
        vector = []
        for index_row in index.rows:
            row = bag.find_row(index_row.term)
            vector.append(row.tf_idf)
        return vector
    #---------------------------------------------------------------- 
    # def getTfIdf(self,)

# ----------------------------------------------------------
    def write_vector_file(self):
      with   open("utilities/vectorCACM/"+str(self.docNum)+".pickle", "wb") as file:
    #   with   open("utilities/vectors/"+str(self.docNum)+".pickle", "wb") as file:
        pickle.dump(self, file ,pickle.HIGHEST_PROTOCOL)
#-----------------------------------------------------------------
    def write_clustvector_file(self):
      with   open("utilities/vectorcluster/"+str(self.docNum)+".pickle", "wb") as file:
    #   with   open("utilities/vectors/"+str(self.docNum)+".pickle", "wb") as file:
        pickle.dump(self, file ,pickle.HIGHEST_PROTOCOL)

####################################################################################################
    def read_vector(docNum):
    
      return pickle.load(open("utilities/vectorCACM/"+str(docNum)+".pickle", "rb"))
    # return pickle.load(open("utilities/vectors/"+str(docNum)+".pickle", "rb"))
#------------------------------------------------------------------
    def read_vector(docNum):
       return pickle.load(open("utilities/vectorcluster/"+str(docNum)+".pickle", "rb"))
    # return pickle.load(open("utilities/vectors/"+str(docNum)+".pickle", "rb"))
               
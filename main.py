import first as q1
import functions as f
import limtizer as q2
import multiformat as q3
import Index as q6
import matching as q7
import autoCorrection as q4
import matchingMethod as match
# import multiformat as q4
import Abbreviations as q5
# glpat-_1QBHiwei7HztfNZtQgp
def text_processor(text):
    #  text = q5.shortcuts_processor(text)
    nounesList = []
    nounesList = q5.pos_tagging(text)
    text =  q1.convert_signs_to_words(text)
    text  = q3.date_processor(text)
    text =  q1.remove_punctuation(text)
    text =  q1.convert_number_to_word(text)
    text =  q1.split_dash_expression(text)
    mylist = q1.tokenize_text(text)
    mylist = q2.lemmatize_verb(mylist)
    mylist = q1.to_lower(mylist)
    mylist = q1.remove_stopwords(mylist,nounesList)
    mylist = q1.stem_words(mylist,nounesList)

    # print(mylist)
    return mylist
# typecorbas=input("enter 1 to search in CISI or 2 to search in CACM: ")  
# corbas=''
# if typecorbas=='1':
#     docsCount = 1460
#     corbas='index'
#     myindex = q6.read_index(corbas)
# elif typecorbas=='2':
#     docsCount = 3204
#     corbas='indexCACM'  
#     myindex = q6.read_index(corbas)

bagsList = []
docsCount = 1460
    # Reading and processing files
createIndex=input("Enter 1 to recreate index, anything else to skip: ")  
# corbas='index'

if createIndex=='1':
 for i in range(1,docsCount+1):
        # text = f.read_file("dataSet/CACMDataSet/"+str(i)+".txt")
        text = f.read_file("dataSet/CISIDataSet/"+str(i)+".txt")
        print(str(i))
        lista = text_processor(text)
        bagsList.append(q6.bag(i, lista))
 # print(text)
 myindex = q6.index(bagsList)
 print(myindex)
 myindex.write_index_file()
 for bag in bagsList:
     bag.calc_tf_idf(myindex.idfDict)
     bag.write_bag_file()

 for i in range(1,docsCount+1):
        vector = q7.vector(i,myindex)
        vector.write_vector_file()

value="Cost and determination of cost associated with systems of automated information."


myindex = q6.read_index()



# correction = q4.correction_suggestion(value)
# print ("did you mean "+str(correction)+"?")
# myindex.print_index()0
# corbas='index'
# print(myindex)
listquery = text_processor(value)   
print(listquery)
matchingFiles = q7.match(listquery, myindex)     
print("Matching Files: "+ str(matchingFiles))


       
from textblob import TextBlob

def correction_suggestion(query):
   queryCorrection = TextBlob(query)
   return str(queryCorrection.correct())
# -----------------------------------------------------------------------------------------------
# test
# -----------------------------------------------------------------------------------------------
# text="the kingdom"
# textCorrection=correction_suggestion(text)
# print("the correct")
# print(textCorrection)

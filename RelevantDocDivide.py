import pickle


rel_set = {}
corbasName='CISIRel'
with open('Query/CISI.REL') as f:
    for l in f.readlines():
        qry_id = l.lstrip(" ").strip("\n").split("\t")[0].split(" ")[0]
        doc_id = l.lstrip(" ").strip("\n").split("\t")[0].split(" ")[-1]
        if qry_id in rel_set:
            rel_set[qry_id].append(int(doc_id))
        else:
            rel_set[qry_id] = []
            rel_set[qry_id].append(int(doc_id))
with open("Query/Rel/"+str(corbasName)+".pickle", "wb") as outp:
  pickle.dump(rel_set,outp,pickle.HIGHEST_PROTOCOL)    
    

# # ------------------------------------------------------------------  
# #   test
# # --------------------------------------------------------------------
# print(rel_set)
# --------------------------------------------------------------------
# --------------------------------------------------------------------


# for  divid the CACM.rel
# one run

rel_set = {}
corbasName='CACMRel'
with open('Query/CACM.REL') as f:
    for l in f.readlines():
        qry_id = int(l.lstrip(" ").strip("\n").split("\t")[0].split(" ")[0])
        doc_id = l.lstrip(" ").strip("\n").split("\t")[0].split(" ")[1]
        print(qry_id)
        print(doc_id)
        if qry_id in rel_set:
            rel_set[qry_id].append(int(doc_id))
        else:
            rel_set[qry_id] = []
            rel_set[qry_id].append(int(doc_id))
with open("Query/Rel/"+str(corbasName)+".pickle", "wb") as outp:
  pickle.dump(rel_set,outp,pickle.HIGHEST_PROTOCOL)
# ------------------------------------------------------------------  
#   test
# --------------------------------------------------------------------
print(rel_set)   
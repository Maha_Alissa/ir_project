from nltk.tokenize import word_tokenize

# nltk.download('wordnet')
from nltk.stem import WordNetLemmatizer
lista="18 Editions of the Dewey Decimal Classifications Comaromi, J.P. The present study is a history of the DEWEY Decimal Classification.  The first edition of the DDC was published in 1876, the eighteenth edition in 1971, and future editions will continue to appear as needed.  In spite of the DDC's long and healthy life, however, its full story has never been told.  There have been biographies of Dewey that briefly describe his system, but this is the first attempt to provide a detailed history of the work that more than any other has spurred the growth of librarianship in this country and abroad. "
def lemmatize_verb(list):
    lemmatizer = WordNetLemmatizer()
    return [lemmatizer.lemmatize(word, pos ='v') for word in list]
def lemmatizer_N(list)    :
    lemmatizer = WordNetLemmatizer()
    return [lemmatizer.lemmatize(word, pos ='n') for word in list]
def lemmatizer(list)  :
     lemmatizer = WordNetLemmatizer()
     return [lemmatizer.lemmatize(word)for word in list]

def tokenize_text(text):
    return word_tokenize(text)
# answer=tokenize_text(lista)
# # answer=lemmatize_verb(answer)
# answer=lemmatizer_N(answer)
# print(answer)

    

    # Lemmatization is the process of grouping together
    #  the different inflected forms of a word so they can be analyzed as a single item.
    # Lemmatization is similar to stemming but it brings context to the words. 
    # So it links words with similar meanings to one word. 
#     Examples of lemmatization:

# -> rocks : rock
# -> corpora : corpus
# -> better : good

# Original Word ---> Root Word (lemma)      Feature

#    meeting    --->   meet                (core-word extraction)
#    was        --->    be                 (tense conversion to present tense)
#    mice       --->   mouse               (plural to singular)
# #############################################################

# 1. Wordnet Lemmatizer  
# Wordnet is a publicly available lexical database of over 200 languages that provides semantic relationships between its words. It is one of the earliest and most commonly used lemmatizer technique.  

# It is present in the nltk library in python.
# Wordnet links words into semantic relations. ( eg. synonyms )
# It groups synonyms in the form of synsets

#############################################################
# 2. Wordnet Lemmatizer (with POS tag) 
# In the above approach, we observed that Wordnet results were not up to the mark. Words like ‘sitting’, ‘flying’ etc remained the same after lemmatization. This is because these words are treated as a noun in the given sentence rather than a verb. To overcome come this, we use POS (Part of Speech) tags. 
# We add a tag with a particular word defining its type (verb, noun, adjective etc). 
# For Example, 
# Word      +    Type (POS tag)     —>     Lemmatized Word
# driving    +    verb      ‘v’            —>     drive
# dogs       +    noun      ‘n’           —>     dog